﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;


namespace InterfazCardiologia
{

    public partial class Form1 : Form
    {
        int flag, tiempo, pulso;
        string lectura;

        public Form1()
        {

            InitializeComponent();
            serialPort2 = new SerialPort();
            CheckForIllegalCrossThreadCalls = false;
            reiniciar();
            

            try
            {
                foreach (string s in System.IO.Ports.SerialPort.GetPortNames())
                {
                    comboBox1.Items.Add(s);
                }
            }             
              
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnInicio_Click(object sender, EventArgs e)
        {
            flag = 1;
            btnInicio.Enabled = false;
            button2.Enabled = true;
            conectionStatus.Text = "Conectado";

            try
            {
                //serialPort2.Close();
                serialPort2.PortName = comboBox1.Text;
                serialPort2.Open();
                serialPort2.DataReceived += new SerialDataReceivedEventHandler(serialPort2_DataReceived);
                btnInicio.Enabled = false;
                EstadoConexion.Text = "Conectado";
                lblSalud.Text = "";
                //serialPort2.Write("1");
                tiempo = 15;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "El puerto COM no puede estar vacio");
                reiniciar();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(flag == 1)
            {
                if(tiempo > 0)
                {
                    tiempo--;

                }
                else
                {
                    flag = 0;
                    int tempPulso = 1; //Convert.ToInt32(lectura) * 4;
                    lblPPM.Text = " " + tempPulso;
                    String tempSalud;

                    if(tempPulso < 60)
                    {
                        tempSalud = "Tu PPM es bajo";
                    }  else if(tempPulso > 100){
                        tempSalud = "Tu PPM es alto";
                    }
                    else
                    {
                        tempSalud = "Tu PPM es normal";
                    }
                    lblSalud.Text = tempSalud;
                    btnInicio.Enabled = true;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            reiniciar();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            serialPort2.Close();
            Application.Exit();
        }


        private void serialPort2_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            int a;


            if (flag == 1)
            {
                lectura = serialPort2.ReadLine();
                a  = Convert.ToInt32(lectura);
                if (a >= 120)
                {
                    if (a % 120 >= 50)
                    {
                        a = (a % 120);
                    }
                    else 
                    {
                        a = 60 + (a % 120);
                    }

                }
                if (a <= 65 && a >= 60)
                {
                    a = 0;
                }


                label8.Text = a.ToString();
                //double temporal = Convert.ToDoublble(lectura)
                // pusla = COnver. Toint32(temporel)

            }
        }

        private void EstadoConexion_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

       

        private void label8_Click(object sender, EventArgs e)
        {

        }

     

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void webBrowser2_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        } 
         
        private void reiniciar()
        {
            serialPort2.Close();
            flag = 0;
            button2.Enabled = false;
            btnInicio.Enabled = true;            
            flag = 0;
            tiempo = 0;
            pulso = 0;
            lectura = "";
            label8.Text = "0";
            conectionStatus.Text = "Desconectado";
            EstadoConexion.Text = "Desconectado";
        }
    }
}
