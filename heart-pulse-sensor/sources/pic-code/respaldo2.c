#include <16f876A.h>
#device adc=10 //ADC de 10 bits
#include <stdio.h>

#fuses XT, NOWDT, NOPROTECT

#use delay (clock=4M)
#use rs232 (baud=9600, parity=N, xmit=pin_c6, rcv=pin_c7, bits=8)

char dato = "";
/*
float read_value = 0;
float volts_value = 0;*/

/*
void pulsaciones() {
  setup_adc_ports(AN0_AN1_AN3);
  setup_adc(adc_clock_div_64);
  set_adc_channel(0);
  delay_us(50);
  read_value = read_adc();
  volts_value = 5.0 * read_value / 1024.0;
  printf("%0.2f\n\r", volts_value);
}*/


#int_rda
void rda_isr() {
  dato = getc();
  switch (dato) {
  case '1':
    output_high(pin_b7); // Prender el LED en rb7 del PIC
    break;
  case '2':
    output_low(pin_b7); // Apagar el LED en rb7 del PIC
    break;
  case '3':
    output_high(pin_b6); // Prender el LED en rb7 del PIC
    break;
  case '4':
    output_low(pin_b6); // Apagar el LED en rb7 del PIC
    break;
  /*case '9':
    pulsaciones();
    break;*/
  }
}


void main() 
{  
  enable_interrupts(int_rda);
  enable_interrupts(global);  

  while (true)
  {
 //pulsaciones();}
  dato = getc();
  switch (dato) {
  case '1':
    output_high(pin_b7); // Prender el LED en rb7 del PIC
    break;
  case '2':
    output_low(pin_b7); // Apagar el LED en rb7 del PIC
    break;
  case '3':
    output_high(pin_b6); // Prender el LED en rb7 del PIC
    break;
  case '4':
    output_low(pin_b6); // Apagar el LED en rb7 del PIC
    break;
  }
}
}

