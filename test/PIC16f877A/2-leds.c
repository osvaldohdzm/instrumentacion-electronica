#include <16f876A.h>
#device adc=10 //ADC de 10 bits
#include <stdio.h>
#fuses XT, NOWDT, NOPROTECT, HS, NOPUT
#use delay (clock=4M)
#use rs232 (baud=9600, parity=N, xmit=pin_c6, rcv=pin_c7, bits=8)

int counter = 0;
char dato = "";


#int_rda
void rda_isr() {
  dato = getc();
  switch (dato) {
  case '1':
    output_high(pin_b7); // Prender el LED en rb7 del PIC
    break;
  case '2':
    output_low(pin_b7); // Apagar el LED en rb7 del PIC
    break;
  case '3':
    output_high(pin_b6); // Prender el LED en rb7 del PIC
    break;
  case '4':
    output_low(pin_b6); // Apagar el LED en rb7 del PIC
    break;
  }
}


void main() 
{  
  enable_interrupts(global);  
  enable_interrupts(int_rda);
  
  while (true)
  {
  }

}

