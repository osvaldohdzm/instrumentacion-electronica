#include <16f877A.h>
#device adc=10 //ADC de 10 bits
#include <stdio.h>
#fuses XT, NOWDT, NOPROTECT, HS, NOPUT
//#use delay (clock=20M)
#use delay (clock=4M)
#use rs232 (baud=9600, parity=N, xmit=pin_c6, rcv=pin_c7, bits=8)

float read_value = 0;

void main() 
{  

  setup_adc_ports(AN0_AN1_AN3);
  setup_adc(adc_clock_div_64);
  
  
  
  while (true)
  {
    set_adc_channel(0); //LM35
    delay_us(20); 
    read_value = read_adc() * 5 * 100 / 1024;
    printf("%.2f\r\n",read_value + 1000 );
    
    /*
    set_adc_channel(1); // RTD acondicinada a 3v 100 �C
        delay_us(20);
    read_value = read_adc();
    */
    
    printf("%.2f\r\n",read_value - (read_value / 25 ) + 2000 );
    
    
    delay_ms(2000);
  }

}

