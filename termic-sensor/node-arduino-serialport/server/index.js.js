const http = require('http');
const express = require('express');
const SocketIO = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = SocketIO.listen(server);

app.use(express.static(__dirname + '/public'));
server.listen(3000, () => console.log('Iniciando servidor en puerto 3000...'));

const SerialPort = require('serialport');
const ReadLine = SerialPort.parsers.Readline;

const port = new SerialPort("COM7", {
  baudRate: 9600
});
const parser = port.pipe(new ReadLine({ delimiter: '\r\n' }));

port.on('open', function () {
  console.log('Conexión establecida!');
});


parser.on('data', function (data) {
  let temp = parseInt(data, 10) + " °C";
  console.log(temp);

  //io.emit('temp', data.toString());
   io.emit('arduino:data', {
    value: data.toString()
  });
});


parser.on('error', (err) => console.log(err));
port.on('error', (err) => console.log(err));