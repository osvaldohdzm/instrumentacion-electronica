const http = require('http');
const express = require('express');
const SocketIO = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = SocketIO.listen(server);

app.use(express.static(__dirname + '/public'));
server.listen(3000, () => console.log('Iniciando servidor en puerto 3000...'));

const SerialPort = require('serialport');
const ReadLine = SerialPort.parsers.Readline;

const port = new SerialPort("COM7", {
  baudRate: 9600
});
const parser = port.pipe(new ReadLine({ delimiter: '\r\n' }));



var valormedido;

port.on('open', function () {
  console.log('Conexión establecida!');

  setTimeout(function() {
		port.write("2", function(err, results) {
                console.log('err ' + err);
                console.log('results ' + results);
            }); 
	}, 2000);

});


parser.on('data', function (data) {
  let temp = parseInt(data, 10) + " cm";
  console.log(temp);

  var maxvalue = 44;
  var minvalue = 11; //11
  valormedido = parseInt(data, 10);

if (valormedido <= minvalue)
  {
  	setTimeout(function() {
		port.write("1", function(err, results) {
                console.log('err ' + err);
                console.log('results ' + results);
            }); 
	}, 2000);
  } else if(valormedido >= 20)
  {
  	setTimeout(function() {
		port.write("2", function(err, results) {
                console.log('err ' + err);
                console.log('results ' + results);
            }); 
	}, 2000);
  }
  
 
   io.emit('arduino:data', {
    value: data.toString()
  });



});


parser.on('error', (err) => console.log(err));
port.on('error', (err) => console.log(err));