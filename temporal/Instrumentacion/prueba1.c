#include <16f876A.h>
#device adc=10 //ADC de 10 bits
#include <stdio.h>

#fuses XT, NOWDT, NOPROTECT

#use delay (clock=4M)

#use rs232 (baud=9600, parity=N, xmit=pin_c6, rcv=pin_c7, bits=8)


#int_rda

//VDD 5 volts, VSS tierra

void pulsaciones();
char dato;
float pulsa=0;

// Values of sensor
float temperature;

long bits;

void rda_isr(){
   dato = getc();
   switch(dato){
      case '1':
         output_high(pin_b7); //Prender el LED en rb7 del PIC
         break;  
      case '2':
         output_low(pin_b7); //Apagar el LED en rb7 del PIC
         break;   
      case '9':
         pulsaciones();
         break;
   }
}

void main(){
   set_tris_A(1);
   set_tris_B(4);
   
   setup_adc_ports(all_analog);
   setup_adc(adc_clock_internal);
   
   while(1)
   {
      set_adc_channel(0);
      delay_ms(1);
      bits=read_adc();
      
      temperature = bits*.488;
      
      if (temperature > 40.3)
      BIT_SET(LED);      
      else
      {
           BIT_CLEAR(LED);
      }
   }
}


void pulsaciones(){
   setup_adc_ports(AN0);
   setup_adc(adc_clock_div_64);
   set_adc_channel(0);
   //Nos va a dar 4.8mV por cada 1 que obtengamos
   pulsa = read_adc()/2;
   
   printf("%0.2f\r\n",pulsa);
}

//Modificar codigo para que funciones con varios canales
